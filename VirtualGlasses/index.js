let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

// gắn kính vào bên trái
let contact = document.getElementById("vglassesList");
let renderGlasses = (list) => {
  let content = "";
  list.map((item) => {
    let ctGlasses = `<div class=" col-4 mt-3"><button class="vglasses__images border-0"><img width="100%" src="${item.src}" alt="" /></button></div>`;
    content += ctGlasses;
    contact.innerHTML = content;
  });
};
renderGlasses(dataGlasses);

// hiện kính lên bên phải
let faceGlasses = (item) => {
  return `<div><img src="${item.virtualImg}" alt="" /></div>`;
};
// show thông tin kính
let glassesInformation = (item) => {
  return `<div class = ""><p>${item.name} - ${item.brand} (${item.color})</p> <span ><button class = "btn btn-danger mr-2">$120</button></span><span class="text-success">Stocking</span> <p class="mt-2">${item.description}</p></div>`;
};
// show kính sang bên phải
let showGlasses = document.getElementById("avatar");
let pickGlasses = document.querySelectorAll(".vglasses__images");
let showInformation = document.getElementById("glassesInfo");
let changeAttributes = document.querySelector(".vglasses__info");
dataGlasses.forEach((item, index) => {
  pickGlasses[index].addEventListener("click", function () {
    showGlasses.innerHTML = faceGlasses(item);
    showInformation.innerHTML = glassesInformation(item);
    showInformation.style.display = "block";
  });
});

let removeGlassesBF = () => {
  document.querySelector(".vglasses__model img").style.display = "none";
  showInformation.style.display = "none";
};
let removeGlassesAT = () => {
  document.querySelector(".vglasses__model img").style.display = "block";
  showInformation.style.display = "block";
};
